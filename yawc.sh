#!/bin/bash

#TODO :
#       regarder les logs quand secretnet
#       Clear passphrase

#Current bug :
#crypt storage a voir                       Tmp fixed

#requirement wpa_supplicant dhcpcd iw
#bonus: macchanger

function Help {
cat << EOF
Usage :
    -r      $(echo -e "\e[4mRandomize\e[0m") the seleted wifi interface MAC adress
    -c      use your $(echo -e "\e[4mCrypt\e[0m") file (previsouly crypt config and store current config into crypt file expect if Amnesia mode enable)
    -s      don't $(echo -e "\e[4mScan\e[0m") for acces point
    -a      $(echo -e "\e[4mAmenisia\e[0m") mode: remove the config ssid/psk once the connection is established
    -m      $(echo -e "\e[4mModify\e[0m") previously config network
    -p      input passphrase in $(echo -e "\e[4mPlain\e[0m") mode
    -h      display this $(echo -e "\e[4mHelp\e[0m") message

Password are stored hashed in ~/.yawc/list, encrypted one in ~/.yawc/list_sec.gpg
Note: One should disable NetworkManager/wicd or any other deamon that might interefer with this script.
EOF
}

function Siface {
    echo -e "Interfaces:"
    for i in `seq 1 $(ls /sys/class/net | grep wl | wc -l)`; do
        echo -ne "$i: "
        ls /sys/class/net | grep wl | sed -n $i\p
    done
    echo -ne "Interface number: "
    read foo
    iface=$(ls /sys/class/net | grep wl | sed -n $foo\p)
    if $(ip link show $iface | grep -q ' DOWN '); then
        if [ "$(id -u)" != "0" ]; then
            echo -e "Selected interface is down, waking it up, sudo needed:"
            alreadyask=1
        if $(sudo ip link set $iface up); then
            :
        else
            phy=$(sudo iw dev | grep -n $iface | awk '{printf substr($1,1,length($1)-1)}')
            phy_number=$(sudo iw dev | head -n $(($phy-1)))
            phy_number="${phy_number: -1}"
            phy_number=$(sudo rfkill list | grep phy$phy_number | awk '{printf substr($1,1,length($1)-1)}')
            if $(sudo rfkill list $phy_number | grep -q 'blocked: yes'); then
                echo -e "Trying to unblock device..."
                sudo rfkill unblock $phy_number
                #Following line are to support WMI module (i.e. dell-wifi) assuming only one per extension is active
                if $(sudo rfkill list | grep -q '\-wifi'); then
                    phy_number=$(sudo rfkill list | grep '\-wifi' | awk '{print $1}' | cut -c 1)
                    sudo rfkill unblock $phy_number
                fi
                if $(sudo rfkill list | grep -q '\-wlan'); then
                    phy_number=$(sudo rfkill list | grep '\-wlan' | awk '{print $1}' | cut -c 1)
                    sudo rfkill unblock $phy_number
                fi
                if $(sudo rfkill list | grep -q '\-wireless'); then
                    phy_number=$(sudo rfkill list | grep '\-wireless' | awk '{print $1}' | cut -c 1)
                    sudo rfkill unblock $phy_number
                fi
            fi
        fi
        fi
    fi
}

function RandoM {
    mac=$(ip link show $iface | grep link | awk '{print $2}')
    sudo ip link set $iface down
    sudo macchanger -r $iface
    sudo ip link set $iface up
    new_mac=$(ip link show $iface | grep link | awk '{print $2}')
    if [ "$mac" == "$new_mac" ]; then
        echo -e "warning : mac adress unchanged"
    else
        randommac=2
    fi
}

function Display {
    echo -e "n°: SSID MAC Signal power Configured"
    for i in `seq 1 $(grep 'SSID' /tmp/yawc/scan | wc -l)`; do
        echo -ne "$i: "
        grep 'SSID' /tmp/yawc/scan | sed -n $i\p | awk '{ if ($2=="") printf "(no_ssid)"; else printf $2;}' && echo -ne "  "
        grep $iface /tmp/yawc/scan | sed -n $i\p | awk '{printf substr($2,1,length($2)-3)}' && echo -ne "  "
        grep 'signal' /tmp/yawc/scan | sed -n $i\p | awk '{printf $2}' && echo -ne " dBm" && echo -ne "  "
        if grep -q $(grep 'SSID' /tmp/yawc/scan | sed -n $i\p | awk '{ if ($2=="") printf "(no_ssid)"; else printf $2;}') /tmp/yawc/list; then
            echo -e "   *   "
        else
            echo -e "       "
        fi
    done
}

function NetworkScan {
    if [ "$(id -u)" != "0" ] && [ "$alreadyask" != "1" ]; then
        echo -e "\nScanning network require sudo or being run as root:"
    fi
    sudo iw dev $iface scan > /tmp/yawc/scan
    echo -ne "\n"
    if [ "$(wc -l /tmp/yawc/scan | awk '{print $1}')" == "0" ];
    then
        echo "No wifi found. Exiting..."
        exit 1
    fi
    column -t <<<$(Display)
}

function NetworkList {
    echo -e "\nPreviously config wifi: "
    for i in `seq 1 $(cat /tmp/yawc/list | wc -l)`; do
        echo -ne "$i: "
        awk '{print $1}' /tmp/yawc/list | sed -n $i\p
    done
}

function NetworkSelect {
    echo -en "\nSSID number [M for hiden network]: "
    read foo
    if [[ $foo =~ ^([M])$ ]]; then
        echo -en "SSID: "
        read ssid
    else
        ssid=$(grep 'SSID' /tmp/yawc/scan | sed -n $foo\p | awk '{print $2}')
    fi
}

function NetworkConfig {
    if grep -q $ssid /tmp/yawc/list && [ "$modify" == "0" ]
    then
        psk=$(grep $ssid /tmp/yawc/list | awk '{print $2}')
        echo -e "network={\n\tssid=\"$ssid\"\n\tpsk=$psk\n}" > /tmp/yawc/config
    else
        echo -en "\nPassphrase: "
        if [ "$plain" = true ]; then
            read psk
        else
            read -s psk
        fi
        wpa_passphrase $ssid $psk > /tmp/yawc/config
        if [ $amnesia == 0 ]; then
            if [ $secretnet == 0 ]; then
                echo $ssid $(grep psk /tmp/yawc/config | sed -n 2p | cut -c 6-) > $HOME/.yawc/list
            else
                rm $HOME/.yawc/list_sec.gpg
                head -n -$(cat $HOME/.yawc/list | wc -l) /tmp/yawc/list > /tmp/yawc/list_sec
                echo $ssid $(grep psk /tmp/yawc/config | sed -n 2p | cut -c 6-) > /tmp/yawc/list_sec
                gpg --cipher-algo AES256 -o $HOME/.yawc/list_sec.gpg -c /tmp/yawc/list_sec #Not convinient, password to be reenter
            fi
        fi
    fi
}

function Connect {
    echo -e "\nConnecting:"
    sudo killall wpa_supplicant &> /dev/null
    sudo wpa_supplicant -B -i$iface -c/tmp/yawc/config
    if [[ -n $(find /usr/bin -name "dhcpcd") ]]; then
        dhcp="dhcpcd"
        sudo pkill dhcpcd
        sudo dhcpcd -n $iface
    else
        dhcp="dhclient"
        sudo pkill dhclient
        sudo dhclient $iface
    fi
    if [ "$(sudo iw $iface link)" != "Not connected."  ]; then
        sudo iw $iface link
    else
        echo -e "\nFailed connecting. Exiting..."
        sudo pkill wpa_supplicant &> /dev/null
        exit 1
    fi
}

function CleanExit {
    rm -rf /tmp/yawc
    if [ "$unusual_exit" = true ]; then
        if [ "$randommac" == "2" ]; then
            sudo ip link set $iface down
            macchanger --mac=$mac $iface &> /dev/null
            echo "Mac Adress reset to original value."
        fi
    fi
    echo "Exited properly."
}

#Main
unusual_exit=true
randommac=0
notscan=0
amnesia=0
secretnet=0
modify=0
plain=false
mkdir /tmp/yawc &> /dev/null
trap "CleanExit" EXIT
mkdir $HOME/.yawc &> /dev/null
touch $HOME/.yawc/list || exit
while getopts "h\?rscdmp" opt; do
    case $opt in
        h|\?)
            Help
            exit 1
            ;;
        r)
            randommac=1
            shift
            ;;
        s)
            notscan=1
            shift
            ;;
        a)
            amnesia=1
            shift
            ;;
        c)
            secretnet=1
            shift
            ;;
        m)
            modify=1
            shift
            ;;
        p)
            plain=true
            shift
            ;;
    esac
done

Siface
if [ $randommac == 1  ]; then
    RandoM
fi
if [ d == 1 ]; then
    if [ -f $HOME/.yawc/list_sec.gpg ]; then
        echo -e "\nCreate your secret encrypted file:"
        touch $HOME/.yawc/list_sec
        gpg --cipher-algo AES256 -o $HOME/.yawc/list_sec.gpg -c $HOME/.yawc/list_sec
        rm $HOME/.yawc/list_sec
        cat $HOME/.yawc/list > /tmp/yawc/list
    else
        gpg --decrypt $HOME/.yawc/list_sec.gpg -o /tmp/yawc/list
        cat $HOME/.yawc/list >> /tmp/yawc/list
    fi
else
    cat $HOME/.yawc/list > /tmp/yawc/list
fi
if [ $notscan == 0 ]; then
    NetworkScan
else
    NetworkList
fi
NetworkSelect
NetworkConfig
Connect
$unusual_exit = false
