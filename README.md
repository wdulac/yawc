# YAWC
## Overview

yawc is a CLI-based wifi client written in bash that is designed to be light, KISS-respectful, and secure. It should work on every Debian or Arch based distribution.

## Installation

Requirements: iw wpa_supplicant macchanger
A dhcp client is also required, dhclient is pre-install on Debian-based distro, dhcpcd on Arch-based distro. This client is implement to work with both of them.
git is not mandatory but used to clone the repo, you can also just copy/paste yawc.sh

Debian:
```
~$ sudo apt-get update && sudo apt-get install iw wpasupplicant macchanger
```

Arch:
```
~$ sudo pacman -S iw wpa_supplicant macchanger
```

Debian & Arch:
```
~$ git clone https://gitlab.com/plut0n/yawc.git
~$ cd yawc
~$ chmod +x yawc.sh
```

## Usage

yawc is build to be easy to use: 
```
~$ ./yawc.sh #normal mode
~$ ./yawc.sh -[rcsamp] #different option avaible
~$ ./yawc.sh -h #display help message
```

Here's the yawc help message:
```
Usage :
    -r      Randomize the seleted wifi interface MAC adress
    -c      use your Crypt file (previsouly crypt config and store current config into crypt file expect if Amnesia mode enable)
    -s      don't Scan for acces point
    -a      Amenisia mode: remove the config ssid/psk once the connection is established
    -m      Modify previously config network
    -p      input passphrase in Plain mode
    -h      display this Help message

Password are stored hashed in ~/.yawc/list, encrypted one in ~/.yawc/list_sec.gpg
Note: One should disable NetworkManager/wicd or any other deamon that might interefer with this script.
```

## Common issues

- Please stop any wifi service such as NetworkManager or wicd. They might interefer with yawc.
- RF-KILL soft blocking a device is no longer a problem (yawc can still wake up a soft-blocked device). However any hard-blocked device won't wake up so mind your hard switch!